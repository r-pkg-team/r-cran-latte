library(latte)

## examples from documentation

test_that("latte_count", {
    expect_equal(
        latte_count(c("x + y <= 10", "x >= 0", "y >= 0")),
        66)
})

test_that("latte_max", {
    expect_equal(
        latte_max("-2 x + 3 y", c("x + y <= 10", "x >= 0", "y >= 0")),
        list(par = c(x = 0L, y = 10L), value = 30L))
})

test_that("latte_min", {
    expect_equal(
        latte_min("-2 x + 3 y", c("x + y <= 10", "x >= 0", "y >= 0")),
        list(par = c(x = 10L, y = 0L), value = -20L))
})

A <- genmodel(varlvls = c(2, 2), facets = list(1, 2))

test_that("genmodel", {
    expect_equal(
        A,
        structure(
            c(1L, 0L, 1L, 0L, 0L, 1L, 1L, 0L, 1L, 0L, 0L, 1L, 0L, 1L, 0L, 1L),
            dim = c(4L, 4L)))
})

test_that("markov", {
    expect_equal(
        markov(A, p = "arb"),
        structure(c(1L, -1L, -1L, 1L), dim = c(4L, 1L)))
})

test_that("zbasis", {
    expect_equal(
        zbasis(A, p = "arb"),
        structure(c(-1L, 1L, 1L, -1L), dim = c(4L, 1L)))
})

test_that("groebner", {
    expect_equal(
        groebner(A, p = "arb"),
        structure(c(-1L, 1L, 1L, -1L), dim = c(4L, 1L)))
})

test_that("graver", {
    expect_equal(
        graver(A),
        structure(c(1L, -1L, -1L, 1L), dim = c(4L, 1L)))
})

test_that("zsolve", {
    mat <- rbind(
        c( 1, -1),
        c(-3,  1),
        c( 1,  1)
    )
    rel <- c("<", "<", ">")
    rhs <- c(2, 1, 1)
    sign <- c(0, 1)
    expect_equal(
        zsolve(mat, rel, rhs, sign),
        list(zinhom = structure(c(2L, 0L, 1L, 1L, 0L, 1L, 0L, 1L),
                                dim = c(4L, 2L)),
             zhom = structure(c(1L, 1L, 1L, 3L, 1L, 2L), dim = 3:2)))
})

test_that("qsolve", {
    mat <- rbind(
        c( 1,  1),
        c( 1,  1)
    )
    rel <- c(">", "<")
    sign <- c(0, 0)
    expect_equal(
        qsolve(mat, rel, sign, p = "arb"),
        list(qhom = structure(integer(0), dim = c(0L, 2L)),
             qfree = structure(c(1L, -1L), dim = 1:2)))
})

test_that("ppi", {
    expect_equal(
        ppi(3),
        structure(c(-2L, 1L, 0L, 0L, -3L, 2L, -1L, -1L,
                    1L, -3L, 0L, 1L, 1L, -2L, 1L),
                  dim = c(3L, 5L)))
    expect_equal(
        t(1:3) %*% ppi(3),
        structure(c(0, 0, 0, 0, 0), dim = c(1L, 5L)))
})
